# Docker wait for mariadb

When you want to start docker compose services only when the mariadb is actually ready you can depend on an instance of this image
https://github.com/compose-spec/compose-spec/blob/master/spec.md#long-syntax-1

## Environment variables

MYSQL_HOST: default `db`
MYSQL_USER: default `root`
MYSQL_PASSWORD or MYSQL_ROOT_PASSWORD: depending on the user configured for connection.
